
public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); // Creates an object to read user input
		Random rand = new Random(); // Creates an object from Random class
		int number = rand.nextInt(10); // generates a number between 0 and 99

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");

		int guess ;
		int sayac = 0;

		while (true) {

			guess = reader.nextInt();
			if (guess == number) {
				System.out.println("doğru bildiniz");
				sayac++;
				break;
			} else if (guess == -1) {
				System.out.println("çıkış yaptınız");
				sayac++;
				break;
			} else {
				if(guess < number) {
					System.out.println("mine is greater  than you  ");
				}
				else {
					System.out.println("mine is less than you");
				}
				sayac++;
				System.out.println("tekrar deneyiniz");
			}

		}
		System.out.println("sayaç =" + sayac);
		reader.close(); // Close the resource before exiting
	}

}
